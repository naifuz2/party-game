import React, { useEffect } from "react";
import { useState } from "react";
import "./ShowWord.css"


function ShowWord() {
    var wordList = require("../words.json").words;
    var [curWord, setCurWord] = useState("");
    var [correctCount, setCorrectCount] = useState(0);

    console.log(wordList);

    function removeWord(word) {
        var idx = wordList.indexOf(word);
        if (idx !== -1) {
            wordList.splice(idx, 1);
        }
    }

    function changeWord() {
        var toremove = wordList[getRandomInt(wordList.length)];
        setCurWord(toremove);
        removeWord(toremove);
    }

    function correctWord() {
        changeWord();
        setCorrectCount(correctCount+1);
    }

    function getRandomInt(max) {
        return Math.floor(Math.random() * max);
    }

    useEffect(() => {
        changeWord();
    },[]);

    return(
        <>
            <div className="word">
                {curWord}
            </div>
            <div className="correct-count">
                Correct Count: {correctCount}
            </div>
            <button className="correct-btn" onClick={correctWord}>Correct</button>
            <button className="pass-btn" onClick={changeWord}>Pass</button>
        </>
    );
}

export default ShowWord;