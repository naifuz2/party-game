import './App.css';
import {BrowserRouter as Router, Routes, Route, Link} from "react-router-dom"; 
import ShowWord from './Components/ShowWord'; 
import AddWord from './Components/AddWord';

function App() {
  return (
    
      <div className="App-header">
      
      <Router>
      <div className=".navbar">
        <nav>
          <ul>
            <li>
              <Link className='link' to="/">Show Word</Link>
            </li>
            <li>
              <Link className='link' to="/add">Add Word</Link>
            </li>
          </ul>
        </nav>
        <Routes>
          <Route path="/" element={<ShowWord />} />
          <Route path="/add" element={<AddWord />} />
        </Routes>
      </div>
      
    </Router>

    </div>
  );
}

export default App;
